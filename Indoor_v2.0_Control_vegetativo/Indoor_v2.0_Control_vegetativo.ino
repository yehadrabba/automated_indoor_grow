#define CAYENNE_PRINT Serial
#include <CayenneMQTTESP8266.h>
#include "DHT.h"
#include <ESP8266WiFi.h>


//-----------------------------------------RED WIFI.-----------------------------------------------------------
char ssid[] = "audiovintage";
char wifiPassword[] = "10215402rabb";

// Cayenne authentication info. This should be obtained from the Cayenne Dashboard.
char username[] = "2c7bc570-874d-11e7-a5d9-9de9b49680ec";
char password[] = "9272e0d26212805161c59c7d4a89834134330c1e";
char clientID[] = "49b0bb00-874d-11e7-a9f6-4b991f8cbdfd";



//------------------- SENSOR DHT21----------------------
#define DHTPIN 12 
#define DHTTYPE DHT21   // DHT 21 (AM2301)

DHT dht(DHTPIN, DHTTYPE);
float hr;
float T;

//------------------HUMEDAD SUELO-----------------------------
int Hs;
int Valor;


//---------------------MUESTREO--------------------

#define LED_PIN 4
//unsigned long Crono=0;
//unsigned int Tiemp = 300000; 
boolean LED_ON = LOW;
boolean state_Luz;
boolean state_Fan;

unsigned long lastMillis = 0;


//---------------------------- LUZ----------------------------
#define LUZ_PIN 02
#define LUZ_ON 14400000      //20 HORAS PRENDIDO
#define LUZ_OFF 72000000     // 4 HORAS APAGADO

unsigned long msLast_luz;    //last time the LED changed state
boolean luzState;        //current LED state
unsigned long ms_luz;        //time from millis() 



//------------------------------VENTILADOR---------------------------------
#define FAN_PIN 05
#define FAN_ON 120000      //2 MINUTOS APAGADO
#define FAN_OFF 900000    // 15 MINUTOS PRENDIDO

unsigned long msLast_fan;    //last time the LED changed state
boolean fanState;        //current LED state
unsigned long ms_fan;        //time from millis()





void setup() {
  Serial.begin(9600);

  dht.begin();
  //Conexion a internet
  Cayenne.begin(username, password, clientID, ssid, wifiPassword);

  //Definiciones de salida
  pinMode(LUZ_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(FAN_PIN, OUTPUT);
}

void loop() {

   Cayenne.loop();
   muestreo();
   
   ms_luz = millis();
   blinkLUZ();

   ms_fan = millis();
   blinkFAN();

}


//-----------------------------------------MUESTREO----------------------------------------------------------------------------

void muestreo()
{
  //SENSOR HUMEDAD Y Tº

    float hr = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float T = dht.readTemperature();

  if (isnan(hr) || isnan(T) ) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }
  
  float hic = dht.computeHeatIndex(T, hr, false);
  
  //SENSOR HUMEDAD SUELO
  Valor = analogRead(0); 
  Hs = map(Valor,0,1023,100,0);

  //LECTURA SALIDAS 
  boolean luz_muestreo = digitalRead(LUZ_PIN);
  boolean fan_muestreo = digitalRead(FAN_PIN);


   if(digitalRead(LUZ_PIN) == 0){
    luz_muestreo = 1;
   }
   else{
    luz_muestreo = 0;
    }
    

 
   if(digitalRead(FAN_PIN) == 0){
    fan_muestreo = 1;
   }
   else{
    fan_muestreo = 0;
    }
   
if(millis() - lastMillis > 60000) {
  lastMillis = millis();
  //ENCENDER LED
    LED_ON = HIGH;
    digitalWrite(LED_PIN, LED_ON);
  

    //MUESTREO CONSOLA
    Serial.print("Humidity: ");
    Serial.println(hr);
    Serial.print("Temperature: ");
    Serial.print(T);
    Serial.println(" *C ");
    Serial.print("Humedad Suelo:");
    Serial.println(Hs);
    Serial.print(" %");
    Serial.print("LUZ = ");Serial.println(luz_muestreo);
    Serial.print("FAN = ");Serial.println(fan_muestreo);
    Serial.println("----------------");
    
        //ENVIO DE DATOS A CAYENNE
    Cayenne.virtualWrite(0, hr);
    Cayenne.virtualWrite(1, T);
    Cayenne.virtualWrite(2, Hs);
    Cayenne.virtualWrite(3, luz_muestreo);
    Cayenne.virtualWrite(4, fan_muestreo);

    
  //Crono=millis();
  }
  
  //APAGAR LED
    LED_ON = LOW;
    digitalWrite(LED_PIN, LED_ON);
}



//TEMPORIZACION LUZ
void blinkLUZ()
{
    if (ms_luz - msLast_luz > (luzState ? LUZ_ON : LUZ_OFF)) {
        digitalWrite(LUZ_PIN, luzState = !luzState);
        msLast_luz = ms_luz;
    }
}


void blinkFAN()
{

  if (ms_fan - msLast_fan > (fanState ? FAN_ON : FAN_OFF)) {
    digitalWrite(FAN_PIN, fanState = !fanState);
    msLast_fan = ms_fan;
    }
}
